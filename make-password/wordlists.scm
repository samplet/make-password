;;; make-password
;;; Copyright 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of make-password.
;;;
;;; make-password is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; make-password is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with make-password.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (make-password wordlists)
  #:use-module (make-password wordlists diceware)
  #:use-module (make-password wordlists eff-large)
  #:use-module (make-password wordlists eff-short-1)
  #:use-module (make-password wordlists eff-short-2)
  #:re-export (wordlist:diceware
               wordlist:eff-large
               wordlist:eff-short-1
               wordlist:eff-short-2))
