;;; make-password
;;; Copyright 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of make-password.
;;;
;;; make-password is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; make-password is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with make-password.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (make-password)
  #:use-module (ice-9 binary-ports)
  #:use-module (make-password wordlists)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-26)
  #:export (make-password))

(define (urandom-bytes n)
  "Read @var{n} bytes from @file{/dev/random} into a bytevector."
  (call-with-input-file "/dev/urandom" (cut get-bytevector-n <> n)))

(define (urandom-u32)
  "Generate a random 32-bit unsigned integer."
  (bytevector-u32-native-ref (urandom-bytes 4) 0))

(define (urandom-integer n)
  "Generate a random non-negative integer less than @var{n} (note that
@var{n} must not exceed 2^32)."
  (when (> n (expt 2 32))
    (error "Cannot generate random integer greater than 2^32."))
  (let* ((q (quotient (expt 2 32) n))
         (max (* q n)))
    (let loop ((m (urandom-u32)))
      (if (< m max)
          (modulo m n)
          (loop (urandom-u32))))))

(define (urandom-element vec)
  "Pick a random element from @var{vec} (note that the length of
@var{vec} must not exceed 2^32)."
  (vector-ref vec (urandom-integer (vector-length vec))))

(define* (make-password #:key (wordlist wordlist:eff-large) (entropy 77))
  "Make a password from @var{wordlist} having at least @var{entropy}
bits of entropy."
  (let ((count (ceiling-quotient entropy
                                 (/ (log (vector-length wordlist))
                                    (log 2)))))
    (string-join (map (lambda _ (urandom-element wordlist))
                      (iota count)))))
