;;; make-password
;;; Copyright 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of make-password.
;;;
;;; make-password is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; make-password is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with make-password.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages man)
             (gnu packages pkg-config)
             (guix build-system gnu)
             ((guix licenses) #:prefix license:)
             (guix packages))

(package
  (name "make-password")
  (version "0.1-rc")
  (source #f)
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("help2man" ,help2man)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,guile-2.2)))
  (home-page #f)
  (synopsis #f)
  (description #f)
  (license license:gpl3+))
